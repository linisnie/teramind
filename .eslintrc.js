module.exports = {
  root: true,
  extends: '@react-native-community',
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  rules: {
    curly: [2, 'multi-line'],
    'react-hooks/exhaustive-deps': 0,
  },
};
