const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const NODE_ENV = process.env.NODE_ENV || 'development';
// require('@babel/polyfill');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// process.traceDeprecation = true;
module.exports = {
  mode: NODE_ENV,
  entry:{
    'js': [
      './src/frontend/index.tsx'
    ]
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        styles: {
          name: 'styles',
          test: /\.css$/,
          chunks: 'all',
          enforce: true
        }
      }
    }
  },
  plugins:[
    // new ExtractTextPlugin({
    //   filename: NODE_ENV === 'development' ? "styles.css" : "styles."+Math.random()+".css"
    // }),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: NODE_ENV === 'development' ? "styles.css" : "[hash].css",
      chunkFilename: NODE_ENV === 'development' ? "[id].css" : '[id].[hash].css'
    }),
    new HtmlWebpackPlugin({
      template: 'src/frontend/index.html',
      filename: __dirname + '/build/public/index.html',
    }),
    // new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    rules: [{
      test: /\.(ts|tsx)?$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ["@babel/react", "@babel/typescript",  "@babel/env"],
          plugins: [
            '@babel/plugin-proposal-object-rest-spread',
            "@babel/plugin-proposal-class-properties",
            "@babel/plugin-syntax-dynamic-import",
          ]
        }
      }
    },
    {
      test: /\.(sa|sc|c)ss$/,
      use: [
        {
          loader: MiniCssExtractPlugin.loader,
          options: {
            // you can specify a publicPath here
            // by default it use publicPath in webpackOptions.output
            publicPath: __dirname + '/build/public/',
            url: false,
            sourceMap: true,
            modules: true,
            importLoaders: true,
          }
        },
        {
          loader: 'css-loader',
          options: {
            // you can specify a publicPath here
            // by default it use publicPath in webpackOptions.output
            publicPath: __dirname + '/build/public/',
            url: false,
            sourceMap: true,
            modules: true,
            importLoaders: true,
          }
        },
        // 'css-loader',
        'sass-loader',
      ],
    },
      {
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        loader: 'graphql-tag/loader'
      }
    ]
  },
  watch: NODE_ENV === 'development',
  watchOptions: {
    aggregateTimeout: 100,
    poll: true,
    ignored: /node_modules/
  },
  devtool: NODE_ENV === 'development' ? 'inline-source-map' : 'inline-source-map',
  resolve: {
    extensions: [ '.ts', '.tsx', '*', '.js', '.jsx', '.graphql']
  },
  output: {
    path: __dirname + '/build/public',
    publicPath: '/',
    filename: NODE_ENV === 'development' ? 'app.js' : 'app.[chunkhash:5].js'
  },
  devServer: {
    contentBase: './build/public',
    hot: true
  }
};


