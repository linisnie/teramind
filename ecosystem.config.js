module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [
    {
      name: 'data_transfer_ap',
      script: './build/server.js',
      output: './logs/pm2/out.log',
      error: './logs/pm2/error.log',
      watch: false,
      ignore_watch: [
        './node_modules',
        './logs',
        './.*',
        './.vscode',
        './public',
      ],
      env: {
        NODE_ENV: 'development',
        ROLLO_ENV: 'test',
        SERVER_IP: '',
        CLUSTER_MODE: true,
      },
      env_production: {
        NODE_ENV: 'production',
        SERVER_IP: '',
        CLUSTER_MODE: true,
        SCHEDULED_WORKS: false,
      },
    },
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy: {
    production: {
      user: 'node',
      host: '212.83.163.1',
      ref: 'origin/master',
      repo: 'git@github.com:repo.git',
      path: '/var/www/production',
      'post-deploy':
        'npm install && pm2 reload ecosystem.config.js --env production',
    },
    dev: {
      user: 'node',
      host: '212.83.163.1',
      ref: 'origin/master',
      repo: 'git@github.com:repo.git',
      path: '/var/www/development',
      'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env dev',
      env: {
        NODE_ENV: 'dev',
      },
    },
  },
};
