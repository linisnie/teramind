import React from 'react';
import { Navigate, useLocation } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import { QGetUser } from '../../scenes/sign-up/core/__generated__/QGetUser';
import { GET_USER } from '../../scenes/sign-up/core/queries';

export function RequireAuth({ children }: { children: JSX.Element }) {
  const { data, loading } = useQuery<QGetUser>(GET_USER, {
    fetchPolicy: 'cache-and-network',
  });
  let location = useLocation();
  if (loading) return null;
  if (!data?.getUser?.login) {
    return <Navigate to="/sign-in" state={{ from: location }} replace />;
  }

  return children;
}
