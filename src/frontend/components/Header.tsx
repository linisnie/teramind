import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import { useMutation, useQuery } from '@apollo/client';
import { QGetUser } from '../scenes/sign-up/core/__generated__/QGetUser';
import { GET_USER, LOGOUT } from '../scenes/sign-up/core/queries';

export const ResponsiveAppBar = () => {
  const { data } = useQuery<QGetUser>(GET_USER);

  const [logout] = useMutation(LOGOUT, {
    refetchQueries: [{ query: GET_USER }],
  });

  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
            <Button href="/" sx={{ my: 2, color: 'white', display: 'block' }}>
              Upload
            </Button>
            <Button
              href="/files"
              sx={{ my: 2, color: 'white', display: 'block' }}>
              Files
            </Button>
          </Box>

          <Box sx={{ flexGrow: 0 }}>
            {data?.getUser?.login ? (
              <Button onClick={() => logout()} color="inherit">
                Logout
              </Button>
            ) : (
              <Button href="/sign-in" color="inherit">
                Login
              </Button>
            )}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default ResponsiveAppBar;
