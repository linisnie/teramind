import React from 'react';
import { render } from 'react-dom';
import { ApolloProvider } from '@apollo/client';
import { App } from './App';
import { client } from './core/apollo';
import { ThemeProvider } from '@mui/material';
import { theme } from './core/material-ui';

render(
  <ApolloProvider client={client}>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </ApolloProvider>,
  document.getElementById('root'),
);
