import React from 'react';
import { FC } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Registration from './scenes/sign-up';
import { RequireAuth } from './components/auth-provider';
import { SignInPage } from './scenes/sign-in';
import { FilesPage } from './scenes/files';
import { Upload } from './scenes/upload-file';
import { DownloadFile } from './scenes/download-file';
import { CssBaseline } from '@mui/material';
import ResponsiveAppBar from './components/Header';
import { NotOwnerPage } from './scenes/not-owner';

export const App: FC = () => {
  return (
    <BrowserRouter>
      <ResponsiveAppBar />
      <CssBaseline />
      <Routes>
        <Route path="/sign-up" element={<Registration />} />
        <Route path="/sign-in" element={<SignInPage />} />
        <Route path="/not-owner" element={<NotOwnerPage />} />
        <Route
          path="/download/:idUser/:fileName"
          element={
            <RequireAuth>
              <DownloadFile />
            </RequireAuth>
          }
        />
        <Route
          path="/files"
          element={
            <RequireAuth>
              <FilesPage />
            </RequireAuth>
          }
        />
        <Route
          path="/"
          element={
            <RequireAuth>
              <Upload />
            </RequireAuth>
          }
        />
      </Routes>
    </BrowserRouter>
  );
};
