/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ResponseErrors } from "./../../../../../__generated__/globalTypes";

// ====================================================
// GraphQL mutation operation: register
// ====================================================

export interface register_register {
  __typename: "Response";
  status: boolean;
  errors: ResponseErrors[] | null;
}

export interface register {
  register: register_register;
}

export interface registerVariables {
  login: string;
  pass: string;
}
