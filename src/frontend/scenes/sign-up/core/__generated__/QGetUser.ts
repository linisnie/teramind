/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: QGetUser
// ====================================================

export interface QGetUser_getUser {
  __typename: "Users";
  idUser: string;
  login: string;
}

export interface QGetUser {
  getUser: QGetUser_getUser | null;
}
