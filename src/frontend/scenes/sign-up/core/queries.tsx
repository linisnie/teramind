import { gql } from '@apollo/client';

export const REGISTER = gql`
  mutation register($login: String!, $pass: String!) {
    register(login: $login, pass: $pass) {
      status
      errors
    }
  }
`;
export const LOGOUT = gql`
  mutation MLogout {
    logout
  }
`;
export const GET_USER = gql`
  query QGetUser {
    getUser {
      idUser
      login
    }
  }
`;
