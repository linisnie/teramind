import React, { useState } from 'react';
import { useMutation } from '@apollo/client';
import { register, registerVariables } from './core/__generated__/register';
import { GET_USER, REGISTER } from './core/queries';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import {
  Avatar,
  Box,
  Button,
  Container,
  Grid,
  Link,
  TextField,
  Typography,
} from '@mui/material';
import { ResponseErrors } from '../../../common/errors-enums';
import { client } from '../../core/apollo';
import { useNavigate } from 'react-router-dom';

enum input {
  login = 'login',
  pass = 'password',
  repeatPass = 'repeat-password',
}

export function Registration() {
  let navigate = useNavigate();
  const [regErrors, setRegErrors] = useState<ResponseErrors[]>([]);
  const [showPassMatchErr, setShowPassMatchErr] = useState(false);
  const [register] = useMutation<register, registerVariables>(REGISTER, {
    onCompleted: async (data) => {
      setRegErrors(data.register.errors || []);
      if (data.register.status) {
        await client.refetchQueries({
          include: [GET_USER],
        });
        navigate('/');
      }
    },
  });

  function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    let formData = new FormData(event.currentTarget);
    let login = formData.get(input.login) as string;
    let pass = formData.get(input.pass) as string;
    let repeatPass = formData.get(input.repeatPass) as string;
    if (pass !== repeatPass) {
      setShowPassMatchErr(true);
      return;
    }
    setShowPassMatchErr(false);
    register({ variables: { login, pass } });
  }

  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}>
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign Up
        </Typography>
        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          <TextField
            error={regErrors.includes(ResponseErrors.loginExists)}
            helperText={
              regErrors.includes(ResponseErrors.loginExists)
                ? 'User with this sign-in already exists'
                : ''
            }
            margin="normal"
            required
            fullWidth
            id={input.login}
            label="Login"
            name={input.login}
            autoComplete="login"
            autoFocus
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name={input.pass}
            label="Password"
            type="password"
            id={input.pass}
          />
          <TextField
            error={showPassMatchErr}
            helperText={
              showPassMatchErr ? 'Please make sure your passwords match' : ''
            }
            margin="normal"
            required
            fullWidth
            name={input.repeatPass}
            label="Repeat password"
            type="password"
            id={input.repeatPass}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}>
            Sign Up
          </Button>
          <Grid container>
            <Grid item>
              <Link href="/sing-in" variant="body2">
                {'Already have an account? Sign in'}
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
  );
}

export default Registration;
