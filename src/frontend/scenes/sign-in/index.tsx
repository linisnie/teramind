import { useLocation, useNavigate } from 'react-router-dom';
import React from 'react';
import { useMutation } from '@apollo/client';
import { LOGIN } from './core/queries';
import { GET_USER } from '../sign-up/core/queries';
import { MLogin, MLoginVariables } from './core/__generated__/MLogin';
import { client } from '../../core/apollo';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import {
  Avatar,
  Box,
  Button,
  Container,
  Grid,
  Link,
  TextField,
  Typography,
} from '@mui/material';
import { ResponseErrors } from '../../../common/errors-enums';

enum input {
  login = 'login',
  pass = 'password',
}

export function SignInPage() {
  let navigate = useNavigate();
  let location = useLocation();
  const [auth, { data: authData }] = useMutation<MLogin, MLoginVariables>(
    LOGIN,
    {
      onCompleted: async (data) => {
        if (data.login.status) {
          // @ts-ignore
          let from = location.state?.from?.pathname || '/';
          await client.refetchQueries({
            include: [GET_USER],
          });
          navigate(from);
          console.log('navigate to', from);
        }
      },
    },
  );

  function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    let formData = new FormData(event.currentTarget);
    let login = formData.get(input.login) as string;
    let pass = formData.get(input.pass) as string;
    auth({ variables: { login, pass } });
  }

  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}>
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          {authData?.login.errors?.includes(
            ResponseErrors.wrongLoginOrPass,
          ) && <Typography>Wrong login or pass</Typography>}
          <TextField
            margin="normal"
            required
            fullWidth
            id={input.login}
            label="Login"
            name="login"
            autoComplete="login"
            autoFocus
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id={input.pass}
            autoComplete="current-password"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}>
            Sign In
          </Button>
          <Grid container>
            <Grid item>
              <Link href="/sign-up" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
  );
}
