import { gql } from '@apollo/client';

export const LOGIN = gql`
  mutation MLogin($login: String!, $pass: String!) {
    login(login: $login, pass: $pass) {
      status
      errors
    }
  }
`;
