/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ResponseErrors } from "./../../../../../__generated__/globalTypes";

// ====================================================
// GraphQL mutation operation: MLogin
// ====================================================

export interface MLogin_login {
  __typename: "Response";
  status: boolean;
  errors: ResponseErrors[] | null;
}

export interface MLogin {
  login: MLogin_login;
}

export interface MLoginVariables {
  login: string;
  pass: string;
}
