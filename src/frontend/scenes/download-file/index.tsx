import React from 'react';
import { useParams } from 'react-router-dom';
import { Container } from '@mui/material';

export function DownloadFile() {
  let params = useParams();
  return (
    <Container style={{ marginTop: 64 }} component="main" maxWidth="xs">
      To download {params.fileName}{' '}
      <a href={`/uploaded-files/${params.idUser}/${params.fileName}`}>
        Click here
      </a>
    </Container>
  );
}
