/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: QGetFiles
// ====================================================

export interface QGetFiles_getFiles {
  __typename: "Files";
  idFile: string;
  fileName: string;
  size: number;
  createdAt: number;
}

export interface QGetFiles {
  getFiles: QGetFiles_getFiles[];
}
