import { gql } from '@apollo/client';

export const GET_FILES = gql`
  query QGetFiles {
    getFiles {
      idFile
      fileName
      size
      createdAt
    }
  }
`;
