import React from 'react';
import { GET_FILES } from './core/queries';
import { useQuery } from '@apollo/client';
import { QGetFiles } from './core/__generated__/QGetFiles';
import { GET_USER } from '../sign-up/core/queries';
import { QGetUser } from '../sign-up/core/__generated__/QGetUser';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Container, Typography } from '@mui/material';
import dayjs from 'dayjs';
import { humanFileSize } from '../../../common/formatting';

export function FilesPage() {
  const { data, loading } = useQuery<QGetFiles>(GET_FILES, {
    fetchPolicy: 'cache-and-network',
  });
  const { data: userData } = useQuery<QGetUser>(GET_USER);
  if (loading) return null;
  if (data?.getFiles.length === 0)
    return (
      <Container style={{ marginTop: 24 }} component="main" maxWidth="lg">
        <Typography align={'center'} component="h1" variant="h5">
          You have not uploaded any files yet.
        </Typography>
      </Container>
    );
  return (
    <Container style={{ marginTop: 24 }} component="main" maxWidth="lg">
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell align="right">Size</TableCell>
              <TableCell align="right">Created at</TableCell>
              <TableCell align="right">Download link</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data?.getFiles.map((file) => (
              <TableRow
                key={file.fileName}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                <TableCell component="th" scope="row">
                  {file.fileName}
                </TableCell>
                <TableCell align="right">{humanFileSize(file.size)}</TableCell>
                <TableCell align="right">
                  {dayjs(file.createdAt).format('HH:mm:ss YYYY.MM.DD')}
                </TableCell>
                <TableCell align="right">
                  <a
                    href={`/uploaded-files/${userData?.getUser?.idUser}/${file.fileName}`}>
                    download
                  </a>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
}
