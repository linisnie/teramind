import { gql } from '@apollo/client';

export const UPLOAD_FILE = gql`
  mutation MUploadFile($file: Upload!) {
    uploadFile(file: $file) {
      fileName
      idFile
      user {
        idUser
      }
    }
  }
`;
