/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: MLogin
// ====================================================

export interface MLogin_login_errors {
  __typename: "LoginError";
  message: string;
}

export interface MLogin_login {
  __typename: "Login";
  status: boolean;
  errors: MLogin_login_errors[];
}

export interface MLogin {
  login: MLogin_login;
}

export interface MLoginVariables {
  login: string;
  pass: string;
}
