/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: QGetFile
// ====================================================

export interface QGetFile_getFile {
  __typename: "Files";
  fileName: string;
  idFile: string;
}

export interface QGetFile {
  getFile: QGetFile_getFile | null;
}

export interface QGetFileVariables {
  fileName: string;
}
