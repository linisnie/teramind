/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: MUploadFile
// ====================================================

export interface MUploadFile_uploadFile_user {
  __typename: "Users";
  idUser: string;
}

export interface MUploadFile_uploadFile {
  __typename: "Files";
  fileName: string;
  idFile: string;
  user: MUploadFile_uploadFile_user | null;
}

export interface MUploadFile {
  uploadFile: MUploadFile_uploadFile;
}

export interface MUploadFileVariables {
  file: Upload;
}
