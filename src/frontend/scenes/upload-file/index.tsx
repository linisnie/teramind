import React, { useCallback, useMemo, useState } from 'react';
import { UPLOAD_FILE } from './core/queries';
import { useMutation } from '@apollo/client';
import { useDropzone } from 'react-dropzone';
import { GET_FILES } from '../files/core/queries';
import {
  MUploadFile,
  MUploadFileVariables,
} from './core/__generated__/MUploadFile';
import { Box, Container, Typography } from '@mui/material';
import { humanFileSize } from '../../../common/formatting';

const baseStyle = {
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '40px',
  borderWidth: 2,
  borderRadius: 2,
  borderColor: '#eeeeee',
  borderStyle: 'dashed',
  backgroundColor: '#fafafa',
  color: '#bdbdbd',
  outline: 'none',
  cursor: 'pointer',
  transition: 'border .24s ease-in-out',
} as React.CSSProperties;

const focusedStyle = {
  borderColor: '#2196f3',
};

export function Upload() {
  return (
    <div>
      <UploadFile />
    </div>
  );
}

const FILE_SIZE_LIMIT = 5 * 10e5;

const UploadFile = () => {
  const [size, setSize] = useState(0);
  const [uploadFile, { loading, data }] = useMutation<
    MUploadFile,
    MUploadFileVariables
  >(UPLOAD_FILE, {
    refetchQueries: [GET_FILES],
  });

  const onDrop = useCallback(
    (acceptedFiles) => {
      const file = acceptedFiles[0];
      console.log(file);
      setSize(file.size);
      console.log(FILE_SIZE_LIMIT);
      if (file.size <= FILE_SIZE_LIMIT)
        uploadFile({
          variables: { file },
        });
    },
    [uploadFile],
  );

  const { getRootProps, getInputProps, isFocused, isDragActive } = useDropzone({
    multiple: false,
    onDrop,
  });

  const style = useMemo(
    () => ({
      ...baseStyle,
      ...(isFocused ? focusedStyle : {}),
      ...(isDragActive ? focusedStyle : {}),
    }),
    [isFocused, isDragActive],
  );

  return (
    <Container component="main" maxWidth="lg">
      <section style={{ marginTop: 24, marginBottom: 24 }}>
        <div {...getRootProps({ style })}>
          <input {...getInputProps()} />
          {loading ? (
            <p>file is uploading now...</p>
          ) : (
            <p>Drag 'n' drop your file here, or click to select manually</p>
          )}
        </div>
      </section>
      <Box>
        {data?.uploadFile.fileName && (
          <div>
            File{' '}
            <span style={{ textDecoration: 'underline' }}>
              {data?.uploadFile.fileName}
            </span>{' '}
            successfully uploaded. Your download link&nbsp;
            <a
              href={`/uploaded-files/${data?.uploadFile?.user?.idUser}/${data?.uploadFile.fileName}`}>
              here
            </a>
          </div>
        )}
        {size > FILE_SIZE_LIMIT && (
          <Typography>
            Maximum allowed file size is 5 MB, your file is{' '}
            {humanFileSize(size)}
          </Typography>
        )}
      </Box>
    </Container>
  );
};
