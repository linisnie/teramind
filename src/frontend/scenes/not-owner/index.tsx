import React from 'react';
import { Container, Typography } from '@mui/material';

export function NotOwnerPage() {
  return (
    <Container style={{ marginTop: 48 }} component="main" maxWidth="lg">
      <Typography align={'center'} component="h1" variant="h5">
        You are not owner of this file. Try login to another account.
      </Typography>
    </Container>
  );
}
