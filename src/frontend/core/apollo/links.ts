import { createUploadLink } from 'apollo-upload-client';
import { onError } from '@apollo/client/link/error';

export const httpLink = createUploadLink({
  credentials: 'include',
});

export const errorLink = onError(
  ({ graphQLErrors, networkError, response }) => {
    if (graphQLErrors) {
      graphQLErrors.forEach(({ message, locations, path }) =>
        console.log(
          `[GraphQL error]: Message: ${message}, Location: ${JSON.stringify(
            locations,
            null,
            2,
          )}, Path: ${path}
        , response: ${JSON.stringify(response, null, 2)}`,
        ),
      );
    }
    if (networkError) console.log(`[Network error]: ${networkError}`);
  },
);
