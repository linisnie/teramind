import { ApolloClient, ApolloLink } from '@apollo/client';
import { errorLink, httpLink } from './links';
import { cache } from './cache';

export const client = new ApolloClient({
  cache,
  link: ApolloLink.from([errorLink, httpLink]),
  credentials: 'include',
});
