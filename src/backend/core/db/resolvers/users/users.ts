import { Arg, Ctx, Mutation, Query, Resolver } from 'type-graphql';
import { Response, Users } from '../../models/users';
import { CustomCtx } from '../../../apollo/context';
import { ResponseErrors } from '../../../../../common/errors-enums';

@Resolver()
export class UserResolver {
  @Mutation(() => Response)
  async login(
    @Arg('login') login: string,
    @Arg('pass') pass: string,
    @Ctx() ctx: CustomCtx,
  ): Promise<Response> {
    let [user] = await Users.find({
      where: {
        login,
        pass,
      },
    });

    const res = new Response();
    if (!user) {
      res.errors = [ResponseErrors.wrongLoginOrPass];
      res.status = false;
    } else {
      res.status = true;
      if (ctx.req.session) {
        ctx.req.session.idUser = user.idUser;
        ctx.req.session.login = user.login;
      }
    }
    return res;
  }

  @Query(() => Users, { nullable: true })
  async getUser(@Ctx() ctx: CustomCtx): Promise<Users | null> {
    if (!ctx.req.session.idUser) return null;
    return Users.findOneOrFail(ctx.req.session.idUser);
  }

  @Mutation(() => Boolean)
  async logout(@Ctx() ctx: CustomCtx): Promise<boolean> {
    if (ctx.req.session) {
      ctx.req.session.idUser = void 0;
    } else {
      console.error('session not found');
    }

    return true;
  }

  @Mutation(() => Response)
  async register(
    @Arg('login') login: string,
    @Arg('pass') pass: string,
    @Ctx() ctx: CustomCtx,
  ): Promise<Response> {
    const res = new Response();
    const [LoginExists] = await Users.find({ where: { login } });
    if (LoginExists) {
      res.errors = [ResponseErrors.loginExists];
      res.status = false;
      return res;
    }
    const user = Users.create({ login, pass });
    await user.save();
    if (ctx.req.session) {
      ctx.req.session.idUser = user.idUser;
      ctx.req.session.login = user.login;
    }
    res.status = true;
    return res;
  }
}
