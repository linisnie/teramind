import { FileResolver } from './file';
import { UserResolver } from './users/users';

const resolvers = [FileResolver, UserResolver];
export { resolvers };
