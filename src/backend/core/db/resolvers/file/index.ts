import { Arg, Authorized, Ctx, Mutation, Query, Resolver } from 'type-graphql';
import { GraphQLUpload, FileUpload } from 'graphql-upload';
import fs from 'fs';
import path from 'path';
import { CustomCtx } from '../../../apollo/context';
import { Files } from '../../models/files';
import { Users } from '../../models/users';
const { finished } = require('stream/promises');

const getDirPath = (userId: string) =>
  path.join(process.env.ROOT_DIR, `/build/public/uploaded-files/${userId}/`);

const getFilePath = (fileName: string, userId: string) =>
  path.join(getDirPath(userId), fileName);

@Resolver()
export class FileResolver {
  @Authorized()
  @Mutation(() => Files)
  async uploadFile(
    @Arg('file', () => GraphQLUpload) file: FileUpload,
    @Ctx() ctx: CustomCtx,
  ): Promise<Files> {
    if (!ctx.idUser) throw Error('user is not authorized');

    const [existingFile] = await Files.find({
      where: { user: ctx.idUser, fileName: file.filename },
      relations: ['user'],
    });
    if (existingFile) {
      console.log(`return an existing file`);
      return existingFile;
    }

    const dirName = getDirPath(ctx.idUser);
    if (!fs.existsSync(dirName)) {
      fs.mkdirSync(dirName, { recursive: true });
    }
    const writeStream: NodeJS.WritableStream = fs.createWriteStream(
      getFilePath(file.filename, ctx.idUser),
    );
    const readStream: NodeJS.ReadableStream = file.createReadStream();
    readStream.pipe(writeStream);

    writeStream.on('error', (err) => {
      console.error(err);
    });

    const out = await finished(writeStream);
    console.log(out);

    const user = await Users.findOneOrFail(ctx.idUser);
    const files = new Files();
    files.fileName = file.filename;
    files.user = user;
    files.size = fs.statSync(getFilePath(file.filename, ctx.idUser)).size;
    await files.save();
    return files;
  }

  @Authorized()
  @Query(() => [Files])
  async getFiles(@Ctx() ctx: CustomCtx): Promise<Files[]> {
    return Files.find({ where: { user: ctx.idUser } });
  }
}

export const isFileOwner = async (idUser: string, idFile: string) => {
  let [file] = await Files.find({ where: { idFile, user: idUser } });
  return !!file;
};
