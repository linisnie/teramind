import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Field, ID, ObjectType, registerEnumType } from 'type-graphql';
import { Files } from './files';
import { ResponseErrors } from '../../../../common/errors-enums';

const NULLABLE = { nullable: true };

@Entity()
@ObjectType()
export class Users extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  idUser!: string;

  @Field(() => String)
  @Index({ unique: true })
  @Column('text')
  login!: string;

  @Field(() => String)
  @Column('text')
  pass!: string;

  @Field(() => [Files], NULLABLE)
  @OneToMany(() => Files, (files) => files.user, NULLABLE)
  files?: Files[];

  @CreateDateColumn()
  createdAt!: Date;
}

registerEnumType(ResponseErrors, {
  name: 'ResponseErrors',
});

@ObjectType()
export class Response extends BaseEntity {
  @Field(() => Boolean)
  status!: boolean;

  @Field(() => [ResponseErrors], { defaultValue: [] })
  errors!: ResponseErrors[];
}
