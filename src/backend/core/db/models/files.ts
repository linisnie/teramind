import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  ManyToOne,
  Column,
  CreateDateColumn,
} from 'typeorm';
import { ObjectType, Field, ID } from 'type-graphql';
import { Users } from './users';

const NULLABLE = { nullable: true };

@Entity()
@ObjectType()
export class Files extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  idFile!: string;

  @Field(() => String)
  @Column('text')
  fileName!: string;

  @Field(() => Number)
  @Column('numeric')
  size!: number;

  @Field(() => Users, NULLABLE)
  @ManyToOne(() => Users, (users) => users.files)
  user!: Users;

  @Field(() => Number)
  @CreateDateColumn()
  createdAt!: Date;
}
