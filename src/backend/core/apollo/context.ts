import { Context, ContextFunction } from 'apollo-server-core';
import { ExpressContext } from 'apollo-server-express/src/ApolloServer';
import express from 'express';

export interface CustomCtx {
  idUser?: string;
  login?: string;
  req: express.Request;
}

export const customContext: ContextFunction<ExpressContext, Context> = async ({
  req,
}) => {
  let idUser = req?.session?.idUser;
  let login = req?.session?.login;
  const ctx: CustomCtx = { idUser, req, login };
  return ctx;
};
