import { AuthChecker } from 'type-graphql';
import { CustomCtx } from '../apollo/context';

export const customAuthChecker: AuthChecker<CustomCtx> = ({ context }) => {
  return context.idUser !== undefined;
};
