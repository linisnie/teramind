import { GraphQLResolveInfo, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type RequireFields<T, K extends keyof T> = { [X in Exclude<keyof T, K>]?: T[X] } & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};

export type GQLFiles = {
  __typename?: 'Files';
  createdAt: Scalars['Float'];
  fileName: Scalars['String'];
  idFile: Scalars['ID'];
  size: Scalars['Float'];
  user?: Maybe<GQLUsers>;
};

export type GQLMutation = {
  __typename?: 'Mutation';
  login: GQLResponse;
  logout: Scalars['Boolean'];
  register: GQLResponse;
  uploadFile: GQLFiles;
};


export type GQLMutationLoginArgs = {
  login: Scalars['String'];
  pass: Scalars['String'];
};


export type GQLMutationRegisterArgs = {
  login: Scalars['String'];
  pass: Scalars['String'];
};


export type GQLMutationUploadFileArgs = {
  file: Scalars['Upload'];
};

export type GQLQuery = {
  __typename?: 'Query';
  getFiles: Array<GQLFiles>;
  getUser?: Maybe<GQLUsers>;
};

export type GQLResponse = {
  __typename?: 'Response';
  errors?: Maybe<Array<GQLResponseErrors>>;
  status: Scalars['Boolean'];
};

export enum GQLResponseErrors {
  LoginExists = 'loginExists',
  WrongLoginOrPass = 'wrongLoginOrPass'
}


export type GQLUsers = {
  __typename?: 'Users';
  files?: Maybe<Array<GQLFiles>>;
  idUser: Scalars['ID'];
  login: Scalars['String'];
  pass: Scalars['String'];
};



export type ResolverTypeWrapper<T> = Promise<T> | T;


export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type LegacyStitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type NewStitchingResolver<TResult, TParent, TContext, TArgs> = {
  selectionSet: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type StitchingResolver<TResult, TParent, TContext, TArgs> = LegacyStitchingResolver<TResult, TParent, TContext, TArgs> | NewStitchingResolver<TResult, TParent, TContext, TArgs>;
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | ResolverWithResolve<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type GQLResolversTypes = {
  Files: ResolverTypeWrapper<GQLFiles>;
  Float: ResolverTypeWrapper<Scalars['Float']>;
  String: ResolverTypeWrapper<Scalars['String']>;
  ID: ResolverTypeWrapper<Scalars['ID']>;
  Mutation: ResolverTypeWrapper<{}>;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
  Query: ResolverTypeWrapper<{}>;
  Response: ResolverTypeWrapper<GQLResponse>;
  ResponseErrors: GQLResponseErrors;
  Upload: ResolverTypeWrapper<Scalars['Upload']>;
  Users: ResolverTypeWrapper<GQLUsers>;
};

/** Mapping between all available schema types and the resolvers parents */
export type GQLResolversParentTypes = {
  Files: GQLFiles;
  Float: Scalars['Float'];
  String: Scalars['String'];
  ID: Scalars['ID'];
  Mutation: {};
  Boolean: Scalars['Boolean'];
  Query: {};
  Response: GQLResponse;
  Upload: Scalars['Upload'];
  Users: GQLUsers;
};

export type GQLFilesResolvers<ContextType = any, ParentType extends GQLResolversParentTypes['Files'] = GQLResolversParentTypes['Files']> = {
  createdAt?: Resolver<GQLResolversTypes['Float'], ParentType, ContextType>;
  fileName?: Resolver<GQLResolversTypes['String'], ParentType, ContextType>;
  idFile?: Resolver<GQLResolversTypes['ID'], ParentType, ContextType>;
  size?: Resolver<GQLResolversTypes['Float'], ParentType, ContextType>;
  user?: Resolver<Maybe<GQLResolversTypes['Users']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type GQLMutationResolvers<ContextType = any, ParentType extends GQLResolversParentTypes['Mutation'] = GQLResolversParentTypes['Mutation']> = {
  login?: Resolver<GQLResolversTypes['Response'], ParentType, ContextType, RequireFields<GQLMutationLoginArgs, 'login' | 'pass'>>;
  logout?: Resolver<GQLResolversTypes['Boolean'], ParentType, ContextType>;
  register?: Resolver<GQLResolversTypes['Response'], ParentType, ContextType, RequireFields<GQLMutationRegisterArgs, 'login' | 'pass'>>;
  uploadFile?: Resolver<GQLResolversTypes['Files'], ParentType, ContextType, RequireFields<GQLMutationUploadFileArgs, 'file'>>;
};

export type GQLQueryResolvers<ContextType = any, ParentType extends GQLResolversParentTypes['Query'] = GQLResolversParentTypes['Query']> = {
  getFiles?: Resolver<Array<GQLResolversTypes['Files']>, ParentType, ContextType>;
  getUser?: Resolver<Maybe<GQLResolversTypes['Users']>, ParentType, ContextType>;
};

export type GQLResponseResolvers<ContextType = any, ParentType extends GQLResolversParentTypes['Response'] = GQLResolversParentTypes['Response']> = {
  errors?: Resolver<Maybe<Array<GQLResolversTypes['ResponseErrors']>>, ParentType, ContextType>;
  status?: Resolver<GQLResolversTypes['Boolean'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export interface GQLUploadScalarConfig extends GraphQLScalarTypeConfig<GQLResolversTypes['Upload'], any> {
  name: 'Upload';
}

export type GQLUsersResolvers<ContextType = any, ParentType extends GQLResolversParentTypes['Users'] = GQLResolversParentTypes['Users']> = {
  files?: Resolver<Maybe<Array<GQLResolversTypes['Files']>>, ParentType, ContextType>;
  idUser?: Resolver<GQLResolversTypes['ID'], ParentType, ContextType>;
  login?: Resolver<GQLResolversTypes['String'], ParentType, ContextType>;
  pass?: Resolver<GQLResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type GQLResolvers<ContextType = any> = {
  Files?: GQLFilesResolvers<ContextType>;
  Mutation?: GQLMutationResolvers<ContextType>;
  Query?: GQLQueryResolvers<ContextType>;
  Response?: GQLResponseResolvers<ContextType>;
  Upload?: GraphQLScalarType;
  Users?: GQLUsersResolvers<ContextType>;
};


