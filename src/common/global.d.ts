import { ReactNativeFile } from 'apollo-upload-client';
import session from 'express-session';

declare module 'express-session' {
  export interface SessionData {
    idUser: string | undefined;
    login: string | undefined;
  }
}

declare global {
  namespace NodeJS {
    export interface ProcessEnv {
      ROOT_DIR: string;
    }
  }
  interface Upload extends ReactNativeFile {}
}
