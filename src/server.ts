import 'reflect-metadata';
import { customAuthChecker } from './backend/core/auth-checker/customAuthChecker';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { customContext } from './backend/core/apollo/context';
import { graphqlUploadExpress } from 'graphql-upload';
import { ApolloServer } from 'apollo-server-express';
import { resolvers } from './backend/core/db/resolvers';
import { createConnection } from 'typeorm';
import { buildSchema } from 'type-graphql';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import { createServer } from 'http';
import express from 'express';
import path from 'path';
import { Users } from './backend/core/db/models/users';
import { Files } from './backend/core/db/models/files';

const PORT = 4000;
process.env.ROOT_DIR = path.join(__dirname, '..');

async function init() {
  await createConnection({
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'Uiol2345',
    database: 'teramind',
    entities: [Users, Files],
    synchronize: true,
    namingStrategy: new SnakeNamingStrategy(),
    logger: void 0,
    logging: void 0,
  });

  const schema = await buildSchema({
    // @ts-ignore
    resolvers,
    authChecker: customAuthChecker,
    emitSchemaFile: path.resolve(
      __dirname,
      '__snapshots__/schema/schema.graphql',
    ),
  });

  const apolloServer = new ApolloServer({
    schema,
    context: customContext,
  });

  await apolloServer.start();

  const publicPath = path.join(process.env.ROOT_DIR, '/build/public');

  const app = express();

  app.use(cookieParser());

  app.use(
    session({
      secret: 'cat',
      resave: false,
      saveUninitialized: true,
    }),
  );

  app.use(graphqlUploadExpress({ maxFileSize: 5000000, maxFiles: 1 }));

  apolloServer.applyMiddleware({ app, path: '/graphql' });

  app.get('/uploaded-files/:userId/:fileName', (req, res, next) => {
    console.log(req?.session?.idUser, req.params.userId);
    if (!req?.session?.idUser) {
      res.redirect(`/download/${req.params.userId}/${req.params.fileName}`);
      return;
    }
    if (req?.session?.idUser === req.params.userId) next();
    else {
      res.redirect(`/not-owner`);
    }
  });

  app.use(express.static(publicPath));

  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '..', '/build/public/index.html'));
  });

  const httpServer = createServer(app);

  httpServer.listen(PORT, () => {
    console.log(
      `🚀 Server ready at http://localhost:${PORT}${apolloServer.graphqlPath}`,
    );
  });
}

(async () => {
  await init();
})();
